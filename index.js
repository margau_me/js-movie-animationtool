const express = require('express')
const app = express()
const fs = require('fs').promises;
const port = 3000
const url = require('url');
const querystring = require('querystring');
const bodyParser = require('body-parser');

app.use(express.static('public'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.set('view engine', 'ejs');
app.set('views','./views');

app.get('/abspann/show', (req, res) => {
  var dur = req.query.dur || 20;
  var color = req.query.color || "000000"
  var inurl = "https://md.margau.net/s/release-notes"
  if(req.query.url != null) {
    inurl = querystring.unescape(req.query.url);
  }
  var mdurl = new url.URL(inurl);
  mdurl.pathname = mdurl.pathname + "/download"
  res.render('abspann/show',{duration: dur, url: mdurl.toString(), color: color});
});

app.get('/abspann/', (req, res) => {
  res.render('abspann/setup');
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})
